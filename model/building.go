package model

import (
	"time"

	"github.com/google/uuid"
)

type Building struct {
	ID            uuid.UUID   `json:"id"`
	ConstructedAt time.Time   `json:"constructedAt"`
	Surface       int32       `json:"surface,omitempty"`
	Addresses     []Address   `json:"addresses,omitempty"`
	Plots         []uuid.UUID `json:"plots,omitempty"`
}

func (building Building) GetID() uuid.UUID { //nolint:gocritic // not necessary
	return building.ID
}

type BuildingCreate struct {
	ID            uuid.UUID `json:"id"`
	ConstructedAt time.Time `json:"constructedAt"`
	Surface       int32     `json:"surface,omitempty"`
}

type BuildingUpdate struct {
	ConstructedAt time.Time `json:"constructedAt"`
	Surface       int32     `json:"surface,omitempty"`
}

type BuildingAttach struct {
	Buildings []uuid.UUID `json:"buildings"`
	Addresses []uuid.UUID `json:"addresses"`
}
