package model

import (
	"github.com/google/uuid"
)

type Municipality struct {
	Code string `json: code`
	Name string `json: name`
}

type Address struct {
	ID                  uuid.UUID    `json:"id"`
	Street              string       `json:"street"`
	HouseNumber         int32        `json:"houseNumber"`
	HouseNumberAddition string       `json:"houseNumberAddition,omitempty"`
	ZipCode             string       `json:"zipCode"`
	Municipality        Municipality `json:"municipality"`
	Purpose             string       `json:"purpose"`
	Surface             int32        `json:"surface"`
	Buildings           []Building   `json:"buildings"`
}
