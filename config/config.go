package config

import (
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

var Config struct {
	Backend struct {
		Domain       string `yaml:"domain"`
		FSCGrantHash string `yaml:"fscGrantHash"`
	} `yaml:"backend"`
	ListenAddress string `yaml:"listenAddress"`
}

func init() {
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		configPath = "config/dev.yaml"
	}

	yamlFile, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatalf("error reading config file: %v", err)
	}

	if err = yaml.Unmarshal(yamlFile, &Config); err != nil {
		log.Fatalf("error unmarshalling config: %v", err)
	}

	if Config.ListenAddress == "" {
		Config.ListenAddress = "0.0.0.0:80" // Default port if not set in config
	}
}
