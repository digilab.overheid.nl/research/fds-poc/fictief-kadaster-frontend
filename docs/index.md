---
title : "Fictief Kadaster frontend"
description: "Frontend to visualize data in registries that the land registry has access to, scoped to the WOZ."
date: 2023-10-27T09:14:40+02:00
draft: false
toc: true
---

## Running locally

Clone this repo. Note: static files (css/scss/js/img) are stored in the `static` dir, but built into the `public` dir.

When running for the first time, make sure `pnmp` is installed (e.g. using `brew install pnpm`) and run `pnpm install`.

Run:

```sh
pnpm run dev
go run main.go
```

This starts a web server locally on the specified port.
