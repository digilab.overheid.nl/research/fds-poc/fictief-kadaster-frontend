package main

import (
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-kadaster-frontend/backend"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-kadaster-frontend/config"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-kadaster-frontend/helpers"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-kadaster-frontend/model"
)

func main() {
	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"formatYear":   helpers.FormatYear,
		"formatDate":   helpers.FormatDate,
		"formatTime":   helpers.FormatTime,
		"numberFormat": helpers.NumberFormat,
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",

		// Override default error handler
		ErrorHandler: func(c *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := fiber.StatusInternalServerError

			// Retrieve the custom status code if it's a *fiber.Error
			var e *fiber.Error
			if errors.As(err, &e) {
				code = e.Code
			}

			// Send custom error page
			if err = c.Render("error", fiber.Map{
				"title":   fmt.Sprintf("Fout %d", code),
				"code":    code,
				"details": err.Error(),
			}); err != nil {
				// In case serving the error page the fails
				return c.Status(code).SendString(err.Error())
			}

			// Return from handler
			return nil
		},
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// Healthz endpoint
	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendString(".") // Send a period as response body, similar to chi's Heartbeat middleware
	})

	// Addresses
	app.Get("/", func(c *fiber.Ctx) error {
		// Fetch the addressess from the Fictief Register Addressen en Gebouwen backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10")) // IMPROVE: use implicitly, do not include in query parameters?
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		var response model.Response[model.Address]
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/addresses?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, &response); err != nil {
			return fmt.Errorf("error fetching addresses: %v", err)
		}

		return c.Render("index", fiber.Map{
			"title":      "Adressen",
			"addresses":  response.Data,
			"pagination": response.Pagination,
		})
	})

	app.Get("/addresses/:id", func(c *fiber.Ctx) error {
		// Fetch the address from the Fictief Register Addressen en Gebouwen backend
		addressID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return fmt.Errorf("uuid parse failed: %w", err)
		}

		address := new(model.Address)
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/addresses/%s", addressID), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, address); err != nil {
			return fmt.Errorf("address request failed: %w", err)
		}

		return c.Render("address-details", fiber.Map{
			"title":   fmt.Sprintf("Adres: %s", addressID.String()),
			"address": address,
		})
	})

	// Buildings
	buildings := app.Group("/buildings")

	buildings.Get("/", func(c *fiber.Ctx) error {
		// Fetch the buildings from the Fictief Register Addressen en Gebouwen backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10"))
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		response := new(model.Response[model.Building])
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/buildings?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, response); err != nil {
			return fmt.Errorf("error fetching buildings: %v", err)
		}

		return c.Render("building-index", fiber.Map{
			"title":      "Gebouwen",
			"buildings":  response.Data,
			"pagination": response.Pagination,
		})
	})

	buildings.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the building from the Fictief Register Addressen en Gebouwen backend
		buildingID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return fmt.Errorf("uuid parse failed: %w", err)
		}

		building := new(model.Building)
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/buildings/%s", buildingID), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, building); err != nil {
			return fmt.Errorf("building request failed: %w", err)
		}

		return c.Render("building-details", fiber.Map{
			"title":    fmt.Sprintf("Adres: %s", buildingID.String()),
			"building": building,
		})
	})

	// Start server
	if err := app.Listen(config.Config.ListenAddress); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
